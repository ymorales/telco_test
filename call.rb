class Call  
  attr_accessor :client, :start_at, :end_at, :call_type, :peak_hour_start

  def initialize(client, start_at, end_at, call_type, peak_hour_start)
    @client          = client
    @start_at        = start_at
    @end_at          = end_at
    @call_type       = call_type
    @peak_hour_start = peak_hour_start
  end

end  