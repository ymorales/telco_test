# How to use #

## Clone the repo
```
$ git clone https://ymorales@bitbucket.org/ymorales/telco_test.git
```

## Run unit tests
```
$ cd telco_test
$ ruby telco_system_test.rb
```