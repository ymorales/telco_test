require_relative 'call'
require_relative 'call_calculator'

class TelcoSystem
    attr_accessor :total, :clients, :historic_calls, :historic_bills, :peak_hour_start

    def initialize(an_hour=nil)
      @total = 0
      @clients = Array.new
      @historic_calls = {}
      @historic_bills = {}
      @peak_hour_start = an_hour
    end

    def self.with_peak_hour_starting_at(an_hour)
      self.new(an_hour)
    end

    def register_client_call(start_date, end_date, client, type)
      call = Call.new(client, start_date, end_date, type, @peak_hour_start)
      bill = CallCalculator.calculateCall(call)
      @total += bill 
      add_call_to_history(call, client)  
      add_call_to_bill_history(bill, client) 
    end  
    
    def register_international_call_between(start_date, end_date, client)
      register_client_call(start_date, end_date, client, 'international')
    end        
    
    def register_national_call_between(start_date, end_date, client)
      register_client_call(start_date, end_date, client, 'national')
    end        
    
    def register_local_call_between(start_date, end_date, client)
      register_client_call(start_date, end_date, client, 'local')             
    end        
    
    def add_call_to_history(call, client)
      if @historic_calls[client].nil?
        @historic_calls[client] = []
        @historic_calls[client].push(call)
      else
        @historic_calls[client].push(call)
      end    
    end  
    
    def add_call_to_bill_history(bill, client)
      if @historic_bills[client].nil?
        @historic_bills[client] = []
        @historic_bills[client].push(bill)
      else
        @historic_bills[client].push(bill)
      end    
    end

    def historical_total_billed
      @total
    end        
    
    def historical_number_of_calls
      total_calls = 0
      @historic_calls.each do |k, v|
        total_calls += v.count
      end
      total_calls  
    end        
    
    def total_billed_for(client)
      @historic_bills[client].inject(0){|sum, x| sum + x }
    end        
    
    def number_of_calls_for(client)
      @historic_calls[client].count
    end        

end