class CallCalculator
  BILL_RULES = {
    international: 2.0,
    national: 1.5,
    local_in_peak_hour: 1.0,
    local_in_not_peak_hour: 0.5 
  }

  def self.calculateCall(call)
    minutes = minutes_in_call(call)
    case (call.call_type)
      when 'local'
        charge =
          if call.end_at.hour == call.peak_hour_start and call.start_at.hour == call.peak_hour_start
            minutes * BILL_RULES[:local_in_peak_hour]
          elsif call.start_at.hour != call.peak_hour_start and call.end_at.hour == call.peak_hour_start
            no_peak_date = Time.local(call.end_at.year, call.end_at.month, call.end_at.day, call.peak_hour_start, 0)  
            first_segment =  (no_peak_date - call.start_at)/60 * BILL_RULES[:local_in_not_peak_hour]

            peak_date = Time.local(call.end_at.year, call.end_at.month, call.end_at.day, call.peak_hour_start, 0)  
            second_segment =  (call.end_at - peak_date)/60 * BILL_RULES[:local_in_peak_hour]
            first_segment + second_segment
          else
            minutes * BILL_RULES[:local_in_not_peak_hour]  
          end
      when 'national'
        charge = minutes * BILL_RULES[:national]
      when 'international'    
        charge = minutes * BILL_RULES[:international]     
    end          
    charge
  end  

  def self.minutes_in_call(call)
    (call.end_at - call.start_at) / 60
  end

end