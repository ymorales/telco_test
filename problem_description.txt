Design and implement a solution to the following problem:

Telephone calls problem:

Model a system which prints the invoices for the customers of a
Telco.

There are 3 different types of calls: local, national and
international.

The cost of an international call is $2 per minute
The cost of a national call is $1.50 per minute
The cost of a local call depends if it was done during peak hour (just one hour) or
not. Peak hour cost is $1 per minute, not peak hour cost is $0.50 per
minute.